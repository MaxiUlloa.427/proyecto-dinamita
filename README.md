# PROYECTO DINAMITA: Menú de funciones (12/10 - 15/10)

##### En el siguiente proyecto se hará un análisis de una función. En primer lugar se realizará un menú solicitando al usuario el tipo de función de la cual posteriormente se pedirá al usuario los parámetros para que el programa calcule los puntos de intersección con el eje de las abscisas (x) y el de las ordenadas (y), las asíntotas y el rango.

##### Realizado por:
- Catalina González: 202130010-4
- Nicolas Verdugo: 202012033-1
- Maximiliano Ulloa: 202130020-1

##### Diagrama de componentes:
